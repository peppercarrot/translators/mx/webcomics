# Episode 25: There Are No Shortcuts

![cover of episode 25](lang/gfx_Pepper-and-Carrot_by-David-Revoy_E25.png)

## Notes

Every five episodes of Pepper&Carrot, I release a muted episode without texts: It's a tough artistic constrain. As if it wasn't constraining enough, it was necessary for me at this point in the story to rebuild the house of Pepper (destroyed in the previous episode), remotivate Pepper to finish her studies and finally showcase the power of the old witches Thym, Cayenne and Cumin (who never really made any demo of their power before this episode). That wasn't easy to pack all of this together, but it feels good to finally release this "firework episode".

Sorry to all the readers of Pepper&Carrot because this episode took me a very long time to produce. It took me time because I wanted to do research on my own painting technique and style. So, I did Freelance works here and there during this period to fund my life and the time streched exponentially. But it was necessary for me to take this time and to study my art again...For me too, "there are no shortcuts"!

I hope you'll like this episode! Thanks again to all my patrons and to all the contributors around the project who helped me to release this episode.
